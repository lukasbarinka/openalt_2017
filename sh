#!/bin/bash

while read
do
	[[ $REPLY =~ \[(.*)-(.*)\].*status\>.*=(.*)\|.*=(.*)\|.*=(.*)\|.*=(.*)\|.*=(.*) ]]	&& STATUS=("${BASH_REMATCH[@]:1}")
done
printf 'DATE\t\t%s\nTIME\t\t%s\nSEMESTER\t%s\nAKTSEM\t\t%s\nSTAVZAP\t\t%s\nCAC_STATUS\t%s\nCAD_STATUS\t%s\n' "${STATUS[@]}" 
