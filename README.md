# WTFs in shell courses teaching

This is a workshop/talk about interesting features in shell and standard UNIX tools. I have learnt these things during my "Programming in shell" courses at university (FIT CTU). The workshop contain one task and many possible solutions.


## About Workshop

This workshop does not have real presentation. The content is stored inside "TOC" file. It is simple text file using TAB for outline (.otl).

The workshop has two parts. The first one contains notes and features of shell and UNIX tools. The second part contains one task with many solutions.

### The task
There is a log file containing many lines with 'status'. These lines describe status of some environment. The status consists of some parts separated by pipe character "|". The goal is to pickup last 'status' line and to transform items into lines with two columns:

```
DATE        2011/02/11
TIME        12:55:45
SEMESTER    B102
AKTSEM      B102
STAVZAP     ZZ
CAC_STATUS  DONE
CAD_STATUS  NOOP
```

There are several solutions for this simple task. The repository contain different aproaches to the solution in different languages/tools:

* dexter - Separate the status line using cut command
* mario - Transform status line using many commands interconnected by pipes
* subst[123] - Substitutions using regular expressions in sed command
* sed - sed in 'porno mode'
* awk - awk in 'porno mode'
* sh - bash in 'porno mode'



## Authors

* **Lukáš Bařinka** - [gitlab](https://gitlab.com/lukasbarinka), [gitlabpages](https://lukasbarinka.gitlab.io)

## License

This project is released under GNU GPLv3 - see the gpl-3.0.txt file for details.


