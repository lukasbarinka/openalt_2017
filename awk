#!/usr/bin/awk -f

/status/ {
	split($0,a,"[->|=[\\]]")
}

END {
	a[4]=a[3]
	a[3]="TIME"
	a[1]="DATE"
	for (i=1;i<=14;i+=2)
		print a[i] "\t" (length(a[i])<8?"\t":"") a[i+1]
}

